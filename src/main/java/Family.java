import java.util.Arrays;

public class Family {
    Human mather;
    Human father;
    Human[] children;
    Pet pet;

    public Human getMather() {
        return mather;
    }

    public void setMather(Human mather) {
        this.mather = mather;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Family(Human mather, Human father, Human[] children) {
        this.mather = mather;
        this.father = father;
        this.children = children;
    }

    public Family(Human mather, Human father) {
        this.mather = mather;
        this.father = father;
    }

    public void addChild(Human human) {
        Human[] childrenBorn = Arrays.copyOf(children, children.length + 1);
        childrenBorn[childrenBorn.length - 1] = human;
        children = new Human[childrenBorn.length];
        children = childrenBorn.clone();

    }

    public boolean deleteChild(int index) {
        if (children.length == 0) {
            return false;
        } else {
            if (children.length - 1 - index >= 0)
                System.arraycopy(children, index + 1, children, index, children.length - 1 - index);
            Human[] childrenDelete = Arrays.copyOf(children, children.length - 1);
            children = new Human[childrenDelete.length];
            children = childrenDelete.clone();
            return true;
        }
    }

    public int countFamily() {
        return 2 + children.length;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mather=" + mather +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }


}
